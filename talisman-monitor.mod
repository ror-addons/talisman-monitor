<?xml version="1.0" encoding="UTF-8"?>
<ModuleFile xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">	
  <UiMod name="talisman-monitor" version="0.6" date="23/12/2010" >		
    <Author name="SHIFTnismo" email="war+talismanmonitor@qube.tv" />		
    <Description text="A window that displays the timer for equiped items talisman." />		
    <VersionSettings gameVersion="1.9.9" windowsVersion="1.0" savedVariablesVersion="1.0" />		
    <Files>			
      <File name="talisman-monitor.lua" />			
      <File name="talisman-monitor.xml" />		
    </Files>		
    <OnInitialize>			
      <CreateWindow name="TalismanMonitorWnd" show="true" />			
      <CallFunction name="TalismanMonitor.Initialize"/>		
    </OnInitialize>		
    <OnShutdown/>	
  </UiMod>
</ModuleFile>