TalismanMonitor = {}

TalismanMonitor.itemsData = {}
TalismanMonitor.labels = {}
TalismanMonitor.empty = 0
TalismanMonitor.showAll = true

function TalismanMonitor.Initialize()
    d("TalismanMonitor Loaded.")

	TalismanMonitor.Setup()
	
	--RegisterEventHandler(SystemData.Events.INTERFACE_RELOADED, "TalismanMonitor.Setup" )
	
	RegisterEventHandler(SystemData.Events.PLAYER_ZONE_CHANGED, "TalismanMonitor.GetValues")
	RegisterEventHandler(SystemData.Events.PLAYER_INVENTORY_SLOT_UPDATED, "TalismanMonitor.GetValues")	
	RegisterEventHandler(SystemData.Events.UPDATE_ITEM_ENHANCEMENT, "TalismanMonitor.GetValues")
	RegisterEventHandler(SystemData.Events.END_ITEM_ENHANCEMENT, "TalismanMonitor.GetValues")
end

function TalismanMonitor.Setup()
	LayoutEditor.RegisterWindow("TalismanMonitorWnd", L"TalismanMonitor", "", true, true, true, nil)
	local texture, x, y = GetIconData(20)	
	if(texture ~= nil) then
	    DynamicImageSetTexture("TalismanMonitorWndICON", texture, x+8, y+8)
	end
	WindowSetShowing("TalismanMonitorWndICON", true)
end

function TalismanMonitor.UpdateIconLabel()
	LabelSetText("TalismanMonitorWndLabel", L""..TalismanMonitor.empty)
end

function TalismanMonitor.ShowStatus()
	Tooltips.CreateTextOnlyTooltip( SystemData.ActiveWindow.name, nil)
    Tooltips.AnchorTooltip( Tooltips.ANCHOR_WINDOW_RIGHT )
    Tooltips.SetTooltipText( 1, 1, L"Talisman Monitor" )
	Tooltips.SetTooltipColor( 1, 1, 163, 53, 255)
	Tooltips.SetTooltipText( 2, 1, TalismanMonitor.GetValues())
	Tooltips.Finalize();
end

function TalismanMonitor.GetEnhValues(myItem)
	local myValues=L""
	for key,value in pairs(myItem) do
		local slot = myItem[key]
		for k,v in pairs(slot.bonus) do
			local duration = slot.bonus[k].duration
			local value = slot.bonus[k].value
			if duration > 0 or TalismanMonitor.showAll then
				if duration == 0 then
					durvalue = L""
				else
					durvalue = TimeUtils.FormatTimeCondensedTruncate(duration)
				end
				myValues = myValues..slot.name..L" [ +"..value..L" "..durvalue..L" ]\n"
			end
		end
	end
	return myValues
end

function TalismanMonitor.GetValues()
	local myValues = L""
	TalismanMonitor.itemsDataHolder = {}
	TalismanMonitor.empty = 0
	for key,value in pairs(GameData.EquipSlots) do
		local item = DataUtils.GetEquipmentData()[value]
		if item.numEnhancementSlots > 0 then 
			if table.getn(item.enhSlot) > 0 then
				myValues = myValues..TalismanMonitor.GetEnhValues(item.enhSlot)
			else
				TalismanMonitor.empty = TalismanMonitor.empty+1
			end
		end
	end
	if TalismanMonitor.empty > 0 then
		myValues = myValues..L"Empty Slots "..L" - "..TalismanMonitor.empty
	end
	TalismanMonitor.UpdateIconLabel()
	return myValues
end

function TalismanMonitor.ToggleShowAll()
	if(TalismanMonitor.showAll) then 
		TalismanMonitor.showAll = false
		EA_ChatWindow.Print(towstring("Now Hiding Permanent Talisman."))
	else
		TalismanMonitor.showAll = true
		EA_ChatWindow.Print(towstring("Now Showing All Talisman."))
	end
end